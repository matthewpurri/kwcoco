.. kwcoco documentation master file, created by
   sphinx-quickstart on Mon Aug  3 12:30:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


:gitlab_url: https://gitlab.kitware.com/computer-vision/kwcoco


Welcome to kwcoco's documentation!
==================================


If you are new, please see our getting started document: :doc:`getting_started <getting_started>` 

Please also see information in the repo `README <https://github.com/Kitware/kwcoco#readme>`_, which 
contains similar but complementary information.

For notes about warping and spaces see :doc:`warping_and_spaces <warping_and_spaces>`.


.. The __init__ files contains the top-level documentation overview
.. automodule:: kwcoco.__init__
   :show-inheritance:


.. toctree::
   :maxdepth: 8
   :caption: Package Layout

   modules
