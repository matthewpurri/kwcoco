kwcoco package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   kwcoco.cli
   kwcoco.data
   kwcoco.demo
   kwcoco.examples
   kwcoco.metrics
   kwcoco.util

Submodules
----------

.. toctree::
   :maxdepth: 4

   kwcoco.abstract_coco_dataset
   kwcoco.category_tree
   kwcoco.channel_spec
   kwcoco.coco_dataset
   kwcoco.coco_evaluator
   kwcoco.coco_image
   kwcoco.coco_objects1d
   kwcoco.coco_schema
   kwcoco.coco_sql_dataset
   kwcoco.compat_dataset
   kwcoco.exceptions
   kwcoco.kpf
   kwcoco.kw18
   kwcoco.sensorchan_spec

Module contents
---------------

.. automodule:: kwcoco
   :members:
   :undoc-members:
   :show-inheritance:
