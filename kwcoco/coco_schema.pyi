from _typeshed import Incomplete


def deprecated(*args):
    ...


def TUPLE(*args, **kw):
    ...


elem: Incomplete
ALLOF: Incomplete
ANY: Incomplete
ANYOF: Incomplete
ARRAY: Incomplete
BOOLEAN: Incomplete
INTEGER: Incomplete
NOT: Incomplete
NULL: Incomplete
NUMBER: Incomplete
OBJECT: Incomplete
ONEOF: Incomplete
STRING: Incomplete
UUID = STRING
PATH = STRING
KWCOCO_KEYPOINT: Incomplete
KWCOCO_POLYGON: Incomplete
ORIG_COCO_KEYPOINTS: Incomplete
KWCOCO_KEYPOINTS: Incomplete
KEYPOINTS: Incomplete
ORIG_COCO_POLYGON: Incomplete
ORIG_COCO_MULTI_POLYGON: Incomplete
POLYGON: Incomplete
RUN_LENGTH_ENCODING: Incomplete
BBOX: Incomplete
SEGMENTATION: Incomplete
CATEGORY: Incomplete
KEYPOINT_CATEGORY: Incomplete
VIDEO: Incomplete
CHANNELS: Incomplete
IMAGE: Incomplete
ANNOTATION: Incomplete
COCO_SCHEMA: Incomplete
